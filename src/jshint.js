'use strict';

module.exports = function (src, options)
{
    return function ()
    {
        var extend = require('extend');

        options = extend({
            cache: 'jshint',
            config: {},
        }, options);

        var gulp  = require('gulp');
        var jshint  = require('gulp-jshint');
        var cached  = require('gulp-cached');
        var stylish = require('jshint-stylish');
        var through = require('through2');
        var notify = require('gulp-notify');
        var handleError = require('./lib/handle-error.js');
        var config = extend({}, require('./config/jshint.json'), options.config);

        if (options.karma) {
            config.node = true;
            config.jasmine = true;

            Array.prototype.push.apply(config.predef, [
                "inject",
            ]);
        }

        if (options.protractor) {
            config.node = true;
            config.jasmine = true;

            Array.prototype.push.apply(config.predef, [
                "by",
                "element",
                "browser",
                "protractor",
            ]);
        }

        var stream = gulp.src(src)
            .pipe(cached(options.cache))
            .pipe(jshint(config))
            .pipe(jshint.reporter(stylish));

        // Use the fail reporter if we are just running "gulp jshint"
        var lastArg = process.argv[process.argv.length - 1];
        if (lastArg === 'jshint') {
            stream.pipe(jshint.reporter('fail'));
        }

        // stream.pipe(notify({
        //     emitError: true,
        //     message: function (file)
        //     {
        //         if (file.jshint.success) {
        //             // Don't show something if success
        //             return false;
        //         }

        //         var errors = file.jshint.results.map(function (data) {
        //             if (data.error) {
        //                 return "(" + data.error.line + ':' + data.error.character + ') ' + data.error.reason;
        //             }
        //         }).join("\n");

        //         return file.relative + " (" + file.jshint.results.length + " errors)\n" + errors;
        //     },
        // }));

        // stream.on('error', handleError);

        return stream;
    };
};
