'use strict';

module.exports = function (src, dest)
{
    return function(done)
    {
        var less = require('./less.js');

        less(src, dest, true)(function()
        {
            var rev = require('rev-hash');
            var fs = require('fs');
            var buffer = fs.readFileSync(dest);
            var hash = rev(buffer);

            fs.renameSync(dest, dest.replace(/\.css$/, '.' + hash + '.css'));
            done();
        });
    };
};
