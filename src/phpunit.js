'use strict';

module.exports = function(cb)
{
    var exec = require('child_process').exec;

    exec('vendor/bin/phpunit', function (err, stdout, stderr)
    {
        stdout && console.log(stdout);
        stderr && console.log(stderr);
        cb(err);
    });
};
