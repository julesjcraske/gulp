'use strict';

module.exports = function (src, dest, minified)
{
    var exec = require('child_process').exec;

    return function(done)
    {
        exec('node_modules/.bin/lessc --source-map ' + (minified ? '--clean-css ' : '') + src + ' ' + dest, done);
    };
};

