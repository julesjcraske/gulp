'use strict';

module.exports = function(src)
{
    return function (cb)
    {
        var phplint = require('phplint').lint;

        phplint(src, {limit: 10}, cb);
    };
};
