'use strict';

module.exports = function (src)
{
    return function (cb)
    {
        var exec = require("child_process").exec;

        var cmd = ['./node_modules/.bin/jsdoc',
            '--recurse ' + src.join(' '),
            '--destination docs/',
            '--readme README.md',
        ].join(' ');

        exec(cmd, cb);
    };
};
