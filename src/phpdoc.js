'use strict';

module.exports = function(src)
{
    return function (cb)
    {
        var fs = require('fs');
        var exec = require('child_process').exec;

        if (!fs.existsSync('./vendor/bin/phpdoc')) {
            console.log('phpdoc is not installed: composer require phpdocumentor/phpdocumentor');
            return;
        }

        console.log('./vendor/bin/phpdoc -d ' + src.join(' -d ') + ' -t docs/api/');
        exec('./vendor/bin/phpdoc -d ' + src.join(' -d ') + ' -t docs/api/', cb);
    };
};
